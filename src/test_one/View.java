package test_one;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Panel;

import javax.swing.JButton;
import javax.swing.JFrame;


public class View {
    
    private JFrame frame;
    
	private JButton redButton;
	private JButton greenButton;
	private JButton blueButton;
    
    public View(){
        frame = new JFrame("Test No.1");                                    
        frame.setLayout(new BorderLayout());                                          
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);           
        frame.setSize(400,200);        
        frame.setVisible(true);
        
        redButton = new JButton("Red");
		greenButton = new JButton("Green");
		blueButton = new JButton("Blue");
        
		Panel panel = new Panel();
		panel.setLayout(new FlowLayout());
		
		panel.add(redButton);    
		panel.add(greenButton);     
		panel.add(blueButton);  
		
		frame.add(panel, BorderLayout.SOUTH);
				
    }
        
    public JButton getRedButton(){
        return redButton;
    }
    
    public JButton getGreenButton(){
        return greenButton;
    }
    
    public JButton getBlueButton(){
        return blueButton;
    }
    
    public void setColorRed(){
        frame.getContentPane().setBackground(Color.RED);
    }
    
    public void setColorGreen(){
    	frame.getContentPane().setBackground(Color.GREEN);
    }
    
    public void setColorBlue(){
    	frame.getContentPane().setBackground(Color.BLUE);
    }
    
}