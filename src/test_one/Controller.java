package test_one;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controller {

    private View view;
    private ActionListener actionListener;
    
    public Controller(View view){
        this.view = view;                 
    }
    
    public void contol(){        
        actionListener = new ActionListener() {
              public void actionPerformed(ActionEvent actionEvent) {                  
            	  view.setColorRed();
              }
        };                
        view.getRedButton().addActionListener(actionListener);   
        
        
        actionListener = new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {                  
            	view.setColorGreen();
            }
        };                
        view.getGreenButton().addActionListener(actionListener); 
        
        
        actionListener = new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {                  
            	 view.setColorBlue();
            }
        };                
        view.getBlueButton().addActionListener(actionListener);   
    }
    
}
