package test_three;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import test_three.View;

public class Controller {
	private View view;
    private ActionListener actionListener;
    
    public Controller(View view){
        this.view = view;                 
    }
    
    public void control(){          
   	 	
   	 	actionListener = new ActionListener() {
	 		public void actionPerformed(ActionEvent actionEvent) {  
	 			
	 			if(view.getRed().isSelected() && !view.getGreen().isSelected() && !view.getBlue().isSelected()){
	 				view.setColorRed();
	 			}else if(!view.getRed().isSelected() && view.getGreen().isSelected() && !view.getBlue().isSelected()){
	 				view.setColorGreen();
	 			}else if(!view.getRed().isSelected() && !view.getGreen().isSelected() && view.getBlue().isSelected()){
	 				view.setColorBlue();
	 			}else if(view.getRed().isSelected() && !view.getGreen().isSelected() && view.getBlue().isSelected()){
	 				view.setColorPurple();
	 			}else if(view.getRed().isSelected() && view.getGreen().isSelected() && !view.getBlue().isSelected()){
	 				view.setColorYellow();
	 			}else if(!view.getRed().isSelected() && view.getGreen().isSelected() && view.getBlue().isSelected()){
	 				view.setColorGreenBlue();
	 			}else if(view.getRed().isSelected() && view.getGreen().isSelected() && view.getBlue().isSelected()){
	 				view.setColorBlack();
	 			}else{
	 				view.setColorBlank();
	 			}
	 				
	 		}
	 	};            
	 	view.getRed().addActionListener(actionListener);
	 	view.getGreen().addActionListener(actionListener);
	 	view.getBlue().addActionListener(actionListener);
  }
    
}
