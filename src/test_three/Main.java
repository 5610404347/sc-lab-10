package test_three;

import javax.swing.SwingUtilities;

import test_three.Controller;
import test_three.View;

public class Main {
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {                                           
           
				View view = new View(); 
				Controller controller = new Controller(view);
				controller.control();
			}
		});  
	}
}