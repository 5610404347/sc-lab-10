package test_three;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Panel;

import javax.swing.JCheckBox;
import javax.swing.JFrame;


public class View {
    private JFrame frame;
    private JCheckBox redChxBox;
    private JCheckBox greenChxBox;
    private JCheckBox blueChxBox;


    public View(){
        frame = new JFrame("Test No.3");                                    
        frame.setLayout(new BorderLayout());                                          
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);           
        frame.setSize(400,200);        
        frame.setVisible(true);
        
        redChxBox = new JCheckBox("Red");
        redChxBox.setBounds(106, 65, 97, 23);
        
        greenChxBox = new JCheckBox("Green");
        greenChxBox.setBounds(106, 99, 97, 23);
        
        blueChxBox = new JCheckBox("Blue");
        blueChxBox.setBounds(106, 133, 97, 23);
        
//        ButtonGroup group = new ButtonGroup();
//        group.add(redChxBox);
//        group.add(greenChxBox);
//        group.add(blueChxBox);
        
        
        Panel panel = new Panel();
		panel.setLayout(new FlowLayout());
		panel.add(redChxBox);
		panel.add(greenChxBox);
		panel.add(blueChxBox);

		frame.add(panel, BorderLayout.SOUTH);
    }
    
	public JCheckBox getRed(){
        return redChxBox;
    }
    
    public JCheckBox getGreen(){
        return greenChxBox;
    }
    
    public JCheckBox getBlue(){
        return blueChxBox;
    }
    
    public void setColorRed(){
        frame.getContentPane().setBackground(Color.RED);
    }
    
    public void setColorGreen(){
    	frame.getContentPane().setBackground(Color.GREEN);
    }
    
    public void setColorBlue(){
    	frame.getContentPane().setBackground(Color.BLUE);
    }
    
    public void setColorBlank(){
        frame.getContentPane().setBackground(null);
    }

	public void setColorYellow() {
		 frame.getContentPane().setBackground(Color.yellow);
		
	}

	public void setColorPurple() {
		 frame.getContentPane().setBackground(new Color(168, 50, 183));
	}

	public void setColorGreenBlue() {
		frame.getContentPane().setBackground(new Color(23, 132, 105));
	}

	public void setColorBlack() {
		frame.getContentPane().setBackground(Color.BLACK);
		
	}

	

}
