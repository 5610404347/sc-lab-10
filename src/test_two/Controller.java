package test_two;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import test_two.View;

public class Controller {
	private View view;
    private ActionListener actionListener;
    
    public Controller(View view){
        this.view = view;                 
    }
    
    public void control(){        
    	 actionListener = new ActionListener() {
             public void actionPerformed(ActionEvent actionEvent) {                  
           	  view.setColorRed();
             }
       };                
       view.getRed().addActionListener(actionListener);   
       
       
       actionListener = new ActionListener() {
           public void actionPerformed(ActionEvent actionEvent) {                  
           	view.setColorGreen();
           }
       };                
       view.getGreen().addActionListener(actionListener); 
       
       
       actionListener = new ActionListener() {
           public void actionPerformed(ActionEvent actionEvent) {                  
           	 view.setColorBlue();
           }
       };                
       view.getBlue().addActionListener(actionListener);   
   }
    
}
