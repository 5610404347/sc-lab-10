package test_two;

import javax.swing.SwingUtilities;

import test_two.Controller;
import test_two.View;

public class Main {
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {                                           
           
				View view = new View(); 
				Controller controller = new Controller(view);
				controller.control();
			}
		});  
	}
}
