package test_two;


import java.awt.BorderLayout;
import java.awt.Color;

import java.awt.FlowLayout;
import java.awt.Panel;

import javax.swing.ButtonGroup;

import javax.swing.JFrame;
import javax.swing.JRadioButton;


public class View {
    
    private JFrame frame;
    
	private JRadioButton redRadio;
	private JRadioButton greenRadio;
	private JRadioButton blueRadio;
    
	private ButtonGroup group;
	
    public View(){
        frame = new JFrame("Test No.2");                                    
        frame.setLayout(new BorderLayout());                                          
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);           
        frame.setSize(400,200);        
        frame.setVisible(true);
        

        redRadio = new JRadioButton("Red");
        redRadio.setBounds(119, 58, 109, 23);
        

        greenRadio = new JRadioButton("Green");
        greenRadio.setBounds(119, 84, 109, 23);
        
        
        blueRadio = new JRadioButton("Blue");
        blueRadio.setBounds(119, 110, 109, 23);
         
        
        ButtonGroup group = new ButtonGroup();
        group.add(redRadio);
        group.add(greenRadio);
        group.add(blueRadio);
        
		Panel panel = new Panel();
		panel.setLayout(new FlowLayout());
		panel.add(redRadio);
		panel.add(greenRadio);
		panel.add(blueRadio);

		
		frame.add(panel, BorderLayout.SOUTH);
				
    }
        
	public JRadioButton getRed(){
        return redRadio;
    }
    
    public JRadioButton getGreen(){
        return greenRadio;
    }
    
    public JRadioButton getBlue(){
        return blueRadio;
    }
    
    public void setColorRed(){
        frame.getContentPane().setBackground(Color.RED);
    }
    
    public void setColorGreen(){
    	frame.getContentPane().setBackground(Color.GREEN);
    }
    
    public void setColorBlue(){
    	frame.getContentPane().setBackground(Color.BLUE);
    }
   
}
