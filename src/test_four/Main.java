package test_four;

import javax.swing.SwingUtilities;

import test_four.Controller;
import test_four.View;

public class Main{

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {                                           
           
				View view = new View(); 
				Controller controller = new Controller(view);
				controller.contol();
			}
		});  
	}

}