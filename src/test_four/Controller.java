package test_four;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controller {

    private View view;
    private ActionListener actionListener;
    
    public Controller(View view){
        this.view = view;                 
    }
    
    public void contol(){        
        actionListener = new ActionListener() {
              public void actionPerformed(ActionEvent actionEvent) {                        	  
            	  String s = (String) view.getComboBox().getSelectedItem();

                  switch (s) {
                      case "Red":
                    	  view.setColorRed();
                          break;
                      case "Green":
                    	  view.setColorGreen();
                          break;
                      case "Blue":
                    	  view.setColorBlue();
                          break;
                  }
            	  
            	  
              }
        };      
               
        view.getComboBox().addActionListener(actionListener);   
        
        

    }
    
}
