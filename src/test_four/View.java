package test_four;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JComboBox;
import javax.swing.JFrame;


public class View {
    
    private JFrame frame;
    
	private JComboBox<String> comboBox;

    
    public View(){
        frame = new JFrame("Test No.4");                                    
        frame.setLayout(new BorderLayout());                                          
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);           
        frame.setSize(400,200);        
        frame.setVisible(true);
        
        String[] colorStrings = { "Red", "Green", "Blue"};
        comboBox = new JComboBox<String>(colorStrings);
        comboBox.setSelectedIndex(2);
        comboBox.setBounds(113, 76, 107, 20);
       
		
		frame.add(comboBox, BorderLayout.SOUTH);
				
    }
        
    public JComboBox<String> getComboBox(){
        return comboBox;
    }
    
    
    public void setColorRed(){
        frame.getContentPane().setBackground(Color.RED);
    }
    
    public void setColorGreen(){
    	frame.getContentPane().setBackground(Color.GREEN);
    }
    
    public void setColorBlue(){
    	frame.getContentPane().setBackground(Color.BLUE);
    }
    
}