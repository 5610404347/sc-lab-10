

public class BankAccount implements Measurable{
	double balance;
	public BankAccount(double balance) { 
		this.balance = balance;
	}
	
	@Override
	public double getMeasure() {
		return balance;
	}

}
