

public class Data {

	public Data() {
		
	}
	public double average (Measurable[] objects){
		double sum = 0;
		for (Measurable measurable : objects) {
				sum += measurable.getMeasure();
		} 
		
		if (objects.length > 0){
			return sum/objects.length;
		}else{
			return 0;
		}
		
	}
	public double min (Measurable[] m){
		double min = m[0].getMeasure();
		
		for (int i = 1; i < m.length; i++) {
			if (m[i].getMeasure() < min) {
				min = m[i].getMeasure();
			}
		}
		
		return min;
		
	}
	
}
