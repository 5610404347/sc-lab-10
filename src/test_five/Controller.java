package test_five;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controller {

    private View view;
    private ActionListener actionListener;
    
    public Controller(View view){
        this.view = view;                 
    }
    
    public void contol(){        
        actionListener = new ActionListener() {
              public void actionPerformed(ActionEvent actionEvent) { 
            	  view.setColorRed();
              }
        };                
        view.getRedItem().addActionListener(actionListener);   
        
        actionListener = new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) { 
          	  view.setColorGreen();
            }
        };                
        view.getGreenItem().addActionListener(actionListener); 
      
        actionListener = new ActionListener() {
          public void actionPerformed(ActionEvent actionEvent) { 
        	  view.setColorBlue();
          }
        };                
        view.getBlueItem().addActionListener(actionListener); 
        

    }
    
}

