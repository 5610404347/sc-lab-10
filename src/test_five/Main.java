package test_five;

import javax.swing.SwingUtilities;

import test_five.Controller;
import test_five.View;

public class Main{

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {                                           
           
				View view = new View(); 
				Controller controller = new Controller(view);
				controller.contol();
			}
		});  
	}

}