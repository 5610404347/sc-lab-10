package test_five;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;


public class View {
    
    private JFrame frame;

	private JMenuBar menuBar;
	private JMenu menu;
	private JMenuItem redItem,greenItem,blueItem;
    
    public View(){
        frame = new JFrame("Test No.5");                                    
        frame.setLayout(new BorderLayout());                                          
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);           
        frame.setSize(400,200);        
        frame.setVisible(true);
        
        menuBar = new JMenuBar();
        frame.setJMenuBar(menuBar);
        menuBar.add(createColorMenu());
    }
        
    private JMenu createColorMenu() {
    	menu = new JMenu("Color");
    	
    	redItem = new JMenuItem("Red");  
    	greenItem = new JMenuItem("Green"); 
    	blueItem = new JMenuItem("Blue"); 
    	
    	menu.add(redItem);
    	menu.add(greenItem);
    	menu.add(blueItem);    	
		return menu;
	}
    

    
    public JMenuItem getRedItem(){ 	
    	return redItem;
    }
    
    public JMenuItem getGreenItem(){ 	
    	return greenItem;
    }
    
    public JMenuItem getBlueItem(){ 	
    	return blueItem;
    }
    
    public void setColorRed(){
        frame.getContentPane().setBackground(Color.RED);
    }
    
    public void setColorGreen(){
    	frame.getContentPane().setBackground(Color.GREEN);
    }
    
    public void setColorBlue(){
    	frame.getContentPane().setBackground(Color.BLUE);
    }
    
}